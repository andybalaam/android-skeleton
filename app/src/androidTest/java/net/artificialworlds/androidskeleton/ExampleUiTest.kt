package net.artificialworlds.androidskeleton

import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.ext.junit.runners.AndroidJUnit4

import org.junit.Test
import org.junit.runner.RunWith

import org.junit.Assert.*


@RunWith(AndroidJUnit4::class)
class ExampleUiTest {
    // TODO: see https://developer.android.com/training/testing/ui-testing/espresso-testing
    @Test
    fun todo() {
        // Context of the app under test.
        val appContext = InstrumentationRegistry.getInstrumentation().targetContext
        assertEquals("net.artificialworlds.androidskeleton", appContext.packageName)
    }
}
