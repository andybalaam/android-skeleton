all: test

test:
	./scripts/update-local-properties
	./gradlew test

connectedAndroidTest:
	./scripts/kill-all-emulators
	./scripts/launch-emulator-and-wait avd-21
	./gradlew connectedAndroidTest
	./scripts/kill-all-emulators
	./scripts/launch-emulator-and-wait avd-29
	./gradlew connectedAndroidTest
