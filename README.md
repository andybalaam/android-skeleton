An example project showing how to make an Android project with reliable,
repeatable tests that run in an emulator.

See the blog post for info:
[Example Android project with repeatable tests running inside an emulator](https://www.artificialworlds.net/blog/2020/05/06/example-android-project-with-repeatable-tests-running-inside-an-emulator/)

## Before you build

Ensure you have Java installed (I used Java 11).

Now run:

```bash
./scripts/install-android-tools
```

## Build

To build and run tests:

```bash
make
```

## Run tests on emulators

To run the "instrumented" tests on two emulators:

```bash
make connectedAndroidTest
```

This depends on a flakey pile of scripts and environment variables that
are inside the `scripts` directory.  Improvements to make those scripts
more reliable would be a welcome contribution to this project.

## Android Studio

You should be able to import the gradle project into Android Studio, but we
avoid checking any Android Studio files in to git.  This helps us ensure the
command-line build is clean and able to run independently and reliably.

The build scripts overwrite the file `local.properties` inside the
android-skeleton directory - this should help Android Studio find the
Android SDK files that are downloaded during the build.  To keep your
environment consistent with our automated builds, we suggest NOT using Android
Studio to download Android SDK components, but instead updating the script
we use to set things up: `scripts/install-android-tools`.

## Developer notes

### Updating Gradle

#### To upgrade gradle:

Edit gradle/wrapper/gradle-wrapper.properties and set the required
gradle version in distributionUrl.

#### To upgrade the android gradle plugin:

Edit build.gradle and in the dependencies section, set the required
gradle plugin version in the entry for
'com.android.tools.build:gradle'.

### Bootstrapping

This is what I ran to start the project from nothing:

```bash
sudo apt install gradle
gradle wrapper --gradle-version=6.3
sudo apt remove gradle
sudo apt autoremove

# I chose Application, Kotlin, Kotlin:
./gradlew init
```

Then I used Android Studio to generate a template project.
